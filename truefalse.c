#include <stdlib.h>
#include <glib.h>

int
main(int argc, char *argv[])
{
	if (argc < 2) {
		g_assert_true(argc < 2);

		return EXIT_SUCCESS;
	}

	g_assert_false(argc < 2);

	return EXIT_FAILURE;
}
