PROGNAME= truefalse
BASE= /usr/local

CFLAGS=  $(shell pkg-config --cflags glib-2.0)
LDFLAGS= $(shell pkg-config --libs glib-2.0)

$(PROGNAME): $(PROGNAME).c
	$(CC) $(CFLAGS) $< -o $@ $(LDFLAGS)

install: $(PROGNAME)
	install $(PROGNAME) $(BASE)/bin

clean:
	rm -f $(PROGNAME)
